import pandas as pd

# Use this to load data. This is far more efficient because we can load data once rather
# than several times for each function.
# Returns array of data loaded from files where each element is data loaded from a file
# e.g. 
# df = get_data()
# df_sitting = df[7][df[7][24] == 1].values --> gets dataset_7.txt and checks for
# activities (24th zero - index) that equals 1
def get_data():
    df_set = []
    for i in range(20):
        df = pd.read_csv('dataset/dataset_' + str(i) + '.txt', sep = ',', header=None)
        df_set.append(df)
    return df_set;