import pandas as pd
import matplotlib.pyplot as plt
from data_loader import get_data

def data_visualization():
    data_files = get_data()
    
    for i in range(20):
        # df = pd.read_csv('dataset/dataset_' + str(i) + '.txt', sep = ',', header=None) 
        # read dataset file. Make this read every file through string concatenation
        # df = pd.read_csv('dataset/dataset_1.txt', sep=',', header=None)
        df_sitting = df[i][df[i][24] == 1].values
        # In this example code, only accelerometer 1 data (column 1 to 3) is used
        plt.plot(df_sitting[:, 0:3])
        plt.show()
        
data_visualization()