import pandas as pd
from scipy import signal
from scipy import stats
import matplotlib.pyplot as plt
import numpy as np
import math
from sklearn import preprocessing
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import make_scorer, accuracy_score, confusion_matrix
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV

activities = [
    "Sitting",
    "Lying",
    "Standing",
    "Washing dishes",
    "Vacuuming",
    "Sweeping",
    "Walking outside",
    "Ascending stairs",
    "Descending stairs",
    "Treadmill running (8.3 km/h)",
    "Bicycling (50 watt)",
    "Bicycling (100 watt)",
    "Rope jumping"
]

# Use this to load data. This is far more efficient because we can load data once rather
# than several times for each function.
# Returns array of data loaded from files where each element is data loaded from a file.
# Each element is a two dimensional array structure.
# e.g. 
# df = get_data()
# df_sitting = df[7][df[7][24] == 1].values --> gets dataset_7.txt and checks for
# activities (24th zero - index) that equals 1
# pandas.read_csv returns a two dimensional array structure, so when the return result of this function
# is used, the first square bracket index gets the two dimensional array structure associated with the file loaded,
# as shown with the example given above.
# Parameters:
#     start_file_number:
#         The number of the first file to read. This number is NOT zero indexed.
#     end_file_number:
#         The number of the last file to read. This number is NOT zero indexed.
def get_data(start_file_number, end_file_number):
    data_file_set = []
    for i in range(start_file_number - 1, end_file_number):
        data_file = pd.read_csv('dataset/dataset_' + str(i + 1) + '.txt', sep = ',', header=None)
        data_file_set.append(data_file)
    return data_file_set;

# Returns visualised plot data in the form of an array of two - dimensional array structures.
# Parameters:
#     data_files:
#         An array of two - dimensional array structures, each of which contains data from a file that has been loaded. 
#     activity:
#         The name of the activity to add.
def visualize_data(data_files, activity):
    data_file_sitting_vals = []
    
    activity_number = activities.index(activity) + 1
    
    for i in range(len(data_files)):
        # df = pd.read_csv('dataset/dataset_' + str(i) + '.txt', sep = ',', header=None) 
        # read dataset file. Make this read every file through string concatenation
        # df = pd.read_csv('dataset/dataset_1.txt', sep=',', header=None)
        df_sitting = data_files[i][data_files[i][24] == activity_number].values # Note to self: values can have the [, ] operator. Not others
        # In this example code, only accelerometer 1 data (column 1 to 3) is used
        plt.plot(df_sitting[:, 0:3])
        data_file_sitting_vals.append(df_sitting)
        plt.show()
    return data_file_sitting_vals

# Returns visualised plot data with removed noise in the form of an array of two - dimensional array structures.
# Parameters:
#     sitting_data_values:
#         An array of two - dimensional array structures
def remove_noise(sitting_data_values):
    data_file_sitting_vals = []
    #df = pd.read_csv('dataset/dataset_1.txt', sep=',', header=None)
    # Butterworth low-pass filter. You could try different parameters and other filters. 
    for i in range(19):
        b, a = signal.butter(4, 0.04, 'low', analog=False)
        #df_sitting = data_files[i][data_files[i][24] == 1].values
        for j in range(3):
            #df_sitting[:,i] = signal.lfilter(b, a, df_sitting[:, i])
            sitting_data_values[i][:, j] = signal.lfilter(b, a, sitting_data_values[i][:, j])
        plt.plot(sitting_data_values[i][:, 0:3])
        data_file_sitting_vals.append(sitting_data_values[i])
        plt.show()
        #plt.plot(sitting_data_values[i][15000:20000, 0:3])
        #plt.show()
    return data_file_sitting_vals

# Uses the given activity_data and engineers features from it. Saves the data in files with given names.
# Does not return (void function)
# Parameters:
#     activity_data:
#         Visualised plot data with removed noise in the form of an array of two - dimensional array structures.
#     training_data_file_name (optional):
#         The name of the training data file. Default is "training_data.csv"
#     testing_data_file_name (optional):
#         The name of the testing data file. Default is "testing_data.csv"
def engineer_features(activity_data, training_data_file_name = "training_data.csv", testing_data_file_name = "testing_data.csv"):
    training = np.empty(shape=(0, 10))
    testing = np.empty(shape=(0, 10))
    # deal with each dataset file
    for i in range(19):
        print('deal with dataset ' + str(i + 1))
        for c in range(1, 14):
            # activity_data = p_noise_removed_data[i][p_noise_removed_data[i][24] == c].values -> It's this line that is the problem
            b, a = signal.butter(4, 0.04, 'low', analog=False)
            for j in range(24):
                activity_data[i][:, j] = signal.lfilter(b, a, activity_data[i][:, j])
            
            datat_len = len(activity_data[i])
            training_len = math.floor(datat_len * 0.8)
            training_data = activity_data[i][:training_len, :]
            testing_data = activity_data[i][training_len:, :]

            # data segementation: for time series data, we need to segment the whole time series, and then extract features from each period of time
            # to represent the raw data. In this example code, we define each period of time contains 1000 data points. Each period of time contains 
            # different data points. You may consider overlap segmentation, which means consecutive two segmentation share a part of data points, to 
            # get more feature samples.
            training_sample_number = training_len // 1000 + 1
            testing_sample_number = (datat_len - training_len) // 1000 + 1

            # To avoid creating multiple variables we could leave sample_data and feature_sample out of the loop
            # They are repeated variables in both loops here.
            # Might be code duplication but there is no guarantee that training_sample_number will be the same as
            # testing_sample_number. Supposedly you could do 
            # for s in range(math.max(training_sample_number, testing_sample_number))
            # but even then you have to deal with the extra unaccounted loops from the min to the max number.
            # Even so, doing that may actually reduce time complexity. Currently two loops with loops in each loop
            # gives time complexity of O(N^2) + O(M^2), whereas with the other solution, it would be O((N + M)^2)
            for s in range(training_sample_number):
                if s < training_sample_number - 1:
                    sample_data = training_data[1000*s:1000*(s + 1), :]
                else:
                    sample_data = training_data[1000*s:, :]
                # in this example code, only three accelerometer data in wrist sensor is used to extract three simple features: min, max, and mean value in
                # a period of time. Finally we get 9 features and 1 label to construct feature dataset. You may consider all sensors' data and extract more

                feature_sample = []
                for i in range(3):
                    feature_sample.append(np.min(sample_data[:, i]))
                    feature_sample.append(np.max(sample_data[:, i]))
                    feature_sample.append(np.mean(sample_data[:, i]))
                feature_sample.append(sample_data[0, -1])
                feature_sample = np.array([feature_sample])
                training = np.concatenate((training, feature_sample), axis=0)
            
            for s in range(testing_sample_number):
                if s < training_sample_number - 1:
                    sample_data = testing_data[1000*s:1000*(s + 1), :]
                else:
                    sample_data = testing_data[1000*s:, :]

                feature_sample = []
                for i in range(3):
                    feature_sample.append(np.min(sample_data[:, i]))
                    feature_sample.append(np.max(sample_data[:, i]))
                    feature_sample.append(np.mean(sample_data[:, i]))
                feature_sample.append(sample_data[0, -1])
                feature_sample = np.array([feature_sample])
                testing = np.concatenate((testing, feature_sample), axis=0)

    df_training = pd.DataFrame(training)
    df_testing = pd.DataFrame(testing)
    df_training.to_csv(training_data_file_name, index=None, header=None)
    df_testing.to_csv(testing_data_file_name, index=None, header=None)

# Reads training and testing data from files with the given file names and creates a model of the data;
# then it evaluates its accuracy and prints it out to the console, as well as the confusion matrix.
# Parameters:
#     training_data_file_name:
#         The name of the training data file. Default is "training_data.csv"
#     testing_data_file_name:
#         The name of the testing data file. Default is "testing_data.csv"   
def model_training_and_evaluation_example(training_data_file_name = "training_data.csv", testing_data_file_name = "testing_data.csv"):
    df_training = pd.read_csv(training_data_file_name, header=None)
    df_testing = pd.read_csv(testing_data_file_name, header=None)
    
    y_train = df_training[9].values
    # Labels should start from 0 in sklearn
    y_train = y_train - 1

    df_training = df_training.drop([9], axis=1)
    X_train = df_training.values

    y_test = df_testing[9].values
    y_test = y_test - 1
    df_testing = df_testing.drop([9], axis=1)
    X_test = df_testing.values
    
    # Feature normalization for improving the performance of machine learning models. In this example code, 
    # StandardScaler is used to scale original feature to be centered around zero. You could try other normalization methods.
    scaler = preprocessing.StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)
    
    # Build KNN classifier, in this example code
    knn = KNeighborsClassifier(n_neighbors=3)
    knn.fit(X_train, y_train)
    
    # Evaluation. when we train a machine learning model on training set, we should evaluate its performance on testing set.
    # We could evaluate the model by different metrics. Firstly, we could calculate the classification accuracy. In this example
    # code, when n_neighbors is set to 4, the accuracy achieves 0.757.
    y_pred = knn.predict(X_test)
    print('Accuracy: ', accuracy_score(y_test, y_pred))
    # We could use confusion matrix to view the classification for each activity.
    print(confusion_matrix(y_test, y_pred))
    
# Execution of tests as shown below.

data_files = get_data(1, 19)

print("Data Visualization:");
visualized_data = visualize_data(data_files, "Sitting")

print("Noise Removal:");
noise_removed_data = remove_noise(visualized_data)

print("Engineer Features:")
engineer_features(noise_removed_data, "training_data.csv", "testing_data.csv")

print("Model Training and Evaluation: ")
model_training_and_evaluation_example("training_data.csv", "testing_data.csv")